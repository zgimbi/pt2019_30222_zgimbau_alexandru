package Polinom;

import java.util.ArrayList;
import java.util.List;


public class Operatii  {
    private Polinom result = new Polinom();

    public void suma(Polinom p1, Polinom p2) {
        result = new Polinom();
        for(Monom m1 :p1.getMonomList()){
            for(Monom m2 :p2.getMonomList()){
                if(m1.getGrad()==m2.getGrad()){
                    result.addMonom(new Monom(m1.getGrad(), m1.getCoef()+m2.getCoef()));
                    m1.setUsed();
                    m2.setUsed();
                }
            }
        }
        for(Monom m: p1.getMonomList()){
            if(m.getUsed()==0){
                result.addMonom(m);
            }
        }
        for(Monom m: p2.getMonomList()){
            if(m.getUsed()==0){
                result.addMonom(m);
            }
        }
    }

    public void inmultire(Polinom p1, Polinom p2) {
        result = new Polinom();
        Monom m;
        int gradMax=p2.getGradPolinom()+p1.getGradPolinom();
        int[] grad = new int[gradMax+1];
        int[] coef = new int[gradMax+1];
        for(Monom m1: p1.getMonomList()){
            for(Monom m2: p2.getMonomList()){
                grad[m1.getGrad()+m2.getGrad()]=1;
                coef[m1.getGrad()+m2.getGrad()]+=m1.getCoef()*m2.getCoef();
            }
        }
        for(int i=0; i<grad.length; i++){
            if(grad[i]!=0)
                result.addMonom(new Monom(i, coef[i]));
        }
    }

    public void diferenta(Polinom p1, Polinom p2) {
        result = new Polinom();
        for(Monom m1 :p1.getMonomList()){
            for(Monom m2 :p2.getMonomList()){
                if(m1.getGrad()==m2.getGrad()){
                    result.addMonom(new Monom(m1.getGrad(), m1.getCoef()-m2.getCoef()));
                    m1.setUsed();
                    m2.setUsed();
                }
            }
        }
        for(Monom m1: p1.getMonomList()){
            if(m1.getUsed()==0)
                result.addMonom(m1);
        }
        for(Monom m: p2.getMonomList()){
            if(m.getUsed()==0)
                result.addMonom(new Monom(m.getGrad(), -m.getCoef()));
        }
    }

    public Polinom derivare(Polinom p){
        result = new Polinom();
        List grad=new ArrayList(p.getGradPolinom()-1);
        for(Monom m: p.getMonomList()){
            if (m.getGrad() >= 1) {
                m.setCoef(m.getCoef()*m.getGrad());
                m.setGrad(m.getGrad() - 1);
            } else {
                m.setCoef(0);
            }
            result.addMonom(m);
        }

        return result;
    }

    public void integrare(Polinom p){
        result = new Polinom();
        for(Monom m: p.getMonomList()){
                result.addMonom(new Monom(m.getGrad()+1, m.getCoef()/(m.getGrad()+1)));
        }
    }

    public void integrare(Polinom p, int a, int b){
        result = new Polinom();
       float val1=0, val2=0, res=0;
       integrare(p);
       for(Monom m : result.getMonomList()){
           val1 += m.getCoef() * Math.pow(b, m.getGrad());
           val2 += m.getCoef() * Math.pow(a, m.getGrad());
       }
       res = val1 - val2;
       result = new Polinom();
       result.addMonom(new Monom(0, res));
    }

    public Monom impartireMonoame(Monom m1, Monom m2){
        float a = m1.getCoef()/m2.getCoef();
        int coef = (int) a;
        Monom m3 = new Monom();
        if(coef!=0)
         m3 = new Monom(m1.getGrad()-m2.getGrad(), coef);
        else
            m3 = new Monom(m1.getGrad()-m2.getGrad(), a);
        return m3;
    }

    public Polinom inmultirePM(Polinom p, Monom m){
        Polinom interm = new Polinom();
        Monom mInterm;
        for(Monom m1 : p.getMonomList()){
            mInterm = new Monom(m1.getGrad()+m.getGrad(), m1.getCoef()*m.getCoef());
            interm.addMonom(mInterm);
        }
        return interm;
    }

    public RezImpartire impartire(Polinom p1, Polinom p2) throws Exception{
      Polinom cat= new Polinom(), rest=new Polinom(), interm = new Polinom();
      Monom m1, m2;
      Monom m3 = new Monom();
      m1 = p1.getMonomList().get(0);
      m2 = p2.getMonomList().get(0);
      if(m1.getGrad()>=m2.getGrad()&& !p2.toString().equals("0")){
          m3 = impartireMonoame(m1, m2);
          cat.addMonom(m3);
          interm = inmultirePM(p2, m3);
          diferenta(p1, interm);
          rest = result;
      }
      else if(m1.getGrad()<m2.getGrad()){
          throw new WrongFormatException("degree(P1)>degree(P2)!");
      }
      else if(p2.toString().equals("0")){
          throw new WrongFormatException("You cannot divide by 0! Cu cine ai facut matematica!?");
      }
      while(rest.getGradPolinom()>=p2.getGradPolinom() && m3.getGrad()!=0 && !rest.toString().isEmpty()){
          m1 = getTopM(rest);
          m3 = impartireMonoame(m1, m2);
          cat.addMonom(m3);
          interm = inmultirePM(p2, m3);
          diferenta(rest, interm);
          rest = result;
      }
      RezImpartire rez = new RezImpartire(cat, rest);
      return rez;
    }
    public Monom getTopM(Polinom p){
        Monom m = null;
        for(Monom mon : p.getMonomList()){
            if(mon.getCoef()!=0){
                m = new Monom (mon.getGrad(), mon.getCoef());
                break;
            }
        }
        return m;
    }
    public Polinom getResult(){
        return result;
    }

}