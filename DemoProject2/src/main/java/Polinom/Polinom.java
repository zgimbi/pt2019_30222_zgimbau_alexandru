package Polinom;

import java.util.ArrayList;
import java.util.List;

public class Polinom {


    private List<Monom> monomList = new ArrayList<Monom>();

    public int getGradPolinom(){
        int gradPolinom=0;
        for(Monom mon: monomList){
            if(gradPolinom<mon.getGrad() && mon.getCoef()!=0)
                gradPolinom=mon.getGrad();
        }
        return gradPolinom;
    }
    public List<Monom> getMonomList() {
        return monomList;
    }

    public void addMonom(Monom m) {
        int index = 0;
        for(Monom monom : monomList){
            if(m.getGrad()>monom.getGrad()){
                index = monomList.indexOf(monom);
                break;
            }
            index++;
        }
        monomList.add(index, m);
    }

    public String toString() {
        String s = "";
        for(Monom m: monomList){
            s = s +m.toString();
        }
        if(s.isEmpty()){
            s = "0";
        }
        return s;
    }

    public void parsePol(String s) throws Exception{
        String[] splitMonoame, split2;
        String emptyString;
        float coef;
        int grad;
        s = s.replaceAll("(--)|(\\+\\+)|(-\\+)|(\\+-)|(x\\d)+|(x\\^x)+", "a");
        emptyString = s.replaceAll("(\\+)|(-)|(\\^)|x|\\d+", "");
        if(!emptyString.isEmpty()){
            throw new WrongFormatException("\nFormat example: 3x^2-6x+5");
        }
        else if(s.isEmpty()){
            throw new WrongFormatException("\nNo poli!");
        }
        else{
            if(s.contains("x")){
                s = s.replaceAll("\\+", " +");
                s = s.replaceAll("-", " -");
                splitMonoame = s.split(" ");
                for (String string : splitMonoame) {
                    System.out.println(string);
                    if (string.isEmpty())
                        continue;
                    String s1;
                    //3 cazuri: --cand string e un numar fara semn in fata =>grad= numar;
                    //          --cand string este empty grad =1;
                    //          -- cand string este un numar cu semn => grad=0;
                    s1 = string.replaceAll("[+-]?\\d+x\\^|[+-]?\\d+x|[+-]?x\\^|[+-]?x|^[+-]{0}\\d+x{0}", "");
                    if (s1.matches("[+-]+\\d"))
                        grad = 0;
                    else if (s1.isEmpty())
                        grad = 1;
                    else
                        grad = Integer.parseInt(s1);
                    s1 = string.replaceAll("(x\\^\\d+)|x", "");
                    if (s1.matches("[+-]?\\d+"))
                        coef = Float.parseFloat(s1);
                    else if (s1.contains("-"))
                        coef = -1;
                    else
                        coef = +1;
                    monomList.add(new Monom(grad, coef));
                }
            }
            else if(!s.contains("x")){
                grad = 0;
                coef = Float.parseFloat(s);
                monomList.add(new Monom(grad, coef));
            }
        }
    }

    }


