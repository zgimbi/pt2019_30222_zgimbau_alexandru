package UI;

import javax.swing.*;
import java.awt.event.ActionListener;

public class UserInterface {
        private JFrame frame = new JFrame("Calculator Polinoame");
        private JLabel labelPolUnu = new JLabel("P1:");
        private JLabel labelPolDoi = new JLabel("P2:");
        private JLabel labelInterval = new JLabel("[a , b]");
        private JTextField tFieldPolUnu = new JTextField();
        private JTextField tFieldPolDoi = new JTextField();
        private JTextField tFieldA = new JTextField("");
        private JTextField tFieldB = new JTextField("");
        private JTextArea textAreaRezultat = new JTextArea("");
        private JButton butonPlus = new JButton("P1 + P2");
        private JButton butonMinus = new JButton("P1 - P2");
        private JButton butonProdus = new JButton("P1 * P2");
        private JButton butonImpartire = new JButton("P1\\P2");
        private JButton butonDerivare = new JButton("  dP/dx  ");
        private JButton butonIntegrare = new JButton("∫P(x)dx");
        private JButton butonIntegrareCapete = new JButton("∫ab");
        private JLabel labelA = new JLabel("a:");
        private JLabel labelB = new JLabel("b:");
        private JLabel labelRez = new JLabel("Result:");

        public UserInterface(){

            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 240);
        frame.setLocation(500, 170);
    //CREARE LAYOUT
            textAreaRezultat.setEditable(false);
            textAreaRezultat.setColumns(2);
            JPanel panel = new JPanel();
            GroupLayout layout = new GroupLayout(panel);
        panel.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(labelPolUnu)
                            .addComponent(labelPolDoi))
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(tFieldPolUnu)
                             .addComponent(tFieldPolDoi)))
                .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(butonPlus)
                                 .addComponent(butonDerivare))
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(butonMinus)
                                .addComponent(butonIntegrare))
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(butonProdus)
                                .addComponent(butonIntegrareCapete))
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(butonImpartire)
                                .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(labelA)
                                            .addComponent(tFieldA))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(labelB)
                                            .addComponent(tFieldB))
                                        .addComponent(labelInterval)
                                )))
                        .addGroup(layout.createSequentialGroup()
                        .addComponent(labelRez)
                        .addComponent(textAreaRezultat))
                        );

        layout.linkSize(SwingConstants.HORIZONTAL, butonPlus, butonDerivare);
        layout.linkSize(SwingConstants.HORIZONTAL, butonMinus, butonIntegrare);
        layout.linkSize(SwingConstants.HORIZONTAL, butonProdus, butonIntegrareCapete);
        layout.linkSize(SwingConstants.VERTICAL, butonDerivare, tFieldA);
        layout.linkSize(SwingConstants.VERTICAL, butonDerivare, tFieldB);
        layout.linkSize(SwingConstants.VERTICAL, butonDerivare, tFieldB);
        layout.linkSize(SwingConstants.HORIZONTAL, butonImpartire, butonDerivare);


        layout.setVerticalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(labelPolUnu)
                        .addComponent(tFieldPolUnu))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(labelPolDoi)
                        .addComponent(tFieldPolDoi))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(butonPlus)
                        .addComponent(butonDerivare))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(butonMinus)
                        .addComponent(butonIntegrare))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(butonProdus)
                        .addComponent(butonIntegrareCapete))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(butonImpartire)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(labelA)
                                .addComponent(tFieldA)
                                .addComponent(labelB)
                                .addComponent(tFieldB)
                                .addComponent(labelInterval))))
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(labelRez)
                        .addComponent(textAreaRezultat))
                );
        frame.getContentPane().add(panel);
        frame.setVisible(true);

    }

    public String getFirstPol(){
            return tFieldPolUnu.getText();
    }

    public String getSecondPol(){
        return tFieldPolDoi.getText();
    }

    public void setTextAreaRezultat(String s){
            textAreaRezultat.setText(s);
    }
    public int getA(){
        return Integer.parseInt(tFieldA.getText());

    }
    public int getB(){
        return Integer.parseInt(tFieldB.getText());
    }

    public void setTFieldA(String s){
            tFieldA.setText(s);
    }
    public void setTFieldB(String s){
            tFieldB.setText(s);
    }
    public void setTFieldPolUnu(String s){
            tFieldPolUnu.setText(s);
    }
    public void setTFieldPolDoi(String s){
        tFieldPolDoi.setText(s);
    }


    public void addButonPlusListener(ActionListener listenerForPlus){
            butonPlus.addActionListener(listenerForPlus);
    }
    public void addButonMinusListener(ActionListener listenerForMinus){
        butonMinus.addActionListener(listenerForMinus);
    }
    public void addButonImpartireListener(ActionListener listenerForImpartire){
        butonImpartire.addActionListener(listenerForImpartire);
    }
    public void addButonDerivareListener(ActionListener listenerForDerivare){
        butonDerivare.addActionListener(listenerForDerivare);
    }
    public void addButonIntegrareListener(ActionListener listenerForIntegrare){
        butonIntegrare.addActionListener(listenerForIntegrare);
    }

    public void addButonIntegrareCapeteListener(ActionListener listenerForIntegrareCapete){
        butonIntegrareCapete.addActionListener(listenerForIntegrareCapete);
    }

    public void addButonProdusListener(ActionListener listenerForProdus){
            butonProdus.addActionListener(listenerForProdus);
    }

}
