import Poli.Operatii;

import Poli.*;
import org.junit.*;
import static org.junit.Assert.*;

public class TestOperatii {

    private static Operatii op;
    private static Polinom p1, p2, p3;
    private static int nrTesteExecutate = 0;

    private static int nrTesteCuSucces = 0;


    public TestOperatii(){

    }

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        op = new Operatii();

    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception{
        System.out.println("S-au executat " + nrTesteExecutate + "teste din care " + nrTesteCuSucces + " au avut succes!");
    }
    @Before
    public void setUp() throws Exception{
        System.out.println("Incepe un nou test!");
        p1 = new Polinom();
        p2 = new Polinom();
        p3 = new Polinom();
        p1.parsePol("12x^4-6x^3+2x+7");
        p2.parsePol("3x^2+2");
        p3.parsePol("0");
        nrTesteExecutate++;
    }
    @After
    public void tearDown() throws Exception{
        System.out.println("S-a terminat testul curent!");
    }

    @Test
    public void testSuma1(){
        op.suma(p1, p2);
        String t = op.getResult().toString();
        assertNotNull(t);
        assertEquals(t, "+12.00x^4-6.00x^3+3.00x^2+2.00x+9.00");
        nrTesteCuSucces++;
    }

    @Test
    public void testSuma2(){
        op.suma(p1, p3);
        String t = op.getResult().toString();
        assertNotNull(t);
        assertEquals(t, "+12.00x^4-6.00x^3+2.00x+7.00");
        nrTesteCuSucces++;
    }

    @Test
    public void testDiferenta1(){
        op.diferenta(p1, p2);
        String t = op.getResult().toString();
        assertNotNull(t);
        assertEquals(t, "+12.00x^4-6.00x^3-3.00x^2+2.00x+5.00");
        nrTesteCuSucces++;
    }

    @Test
    public void testInmultire1(){
        op.suma(p1, p3);
        String t = op.getResult().toString();
        assertNotNull(t);
        assertEquals(t, "+12.00x^4-6.00x^3+2.00x+7.00");
        nrTesteCuSucces++;
    }

    @Test
    public void testInmultire2(){
        op.inmultire(p1, p3);
        String t = op.getResult().toString();
        assertNotNull(t);
        assertEquals(t, "0");
        nrTesteCuSucces++;
    }

    @Test
    public void testImpartire1(){
        RezImpartire res;
        try {
            res = op.impartire(p1, p2);
            String t1, t2;
            t1 = res.getCat().toString();
            t2 = res.getRest().toString();
            assertNotNull(t1);
            assertNotNull(t2);
            assertEquals(t1, "+4.00x^2-2.00x-2.00");
            assertEquals(t2, "-2.00x^2+6.00x+11.00");
            nrTesteCuSucces++;
        }
        catch (WrongFormatException ex){
            fail("Fail impartire!");
        }
        catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testImpartire2(){
        RezImpartire res;
        try {
            res = op.impartire(p1, p3);
            fail("Esuat! Div by 0?");
        }
        catch (WrongFormatException ex){
            assertTrue(true);
            nrTesteCuSucces++;
        }
        catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testIntegrare(){
        String t;
        op.integrare(p1);
        t = op.getResult().toString();
        assertNotNull(t);
        assertEquals(t, "+2.40x^5-1.50x^4+1.00x^2+7.00x");
        nrTesteCuSucces++;
    }

    @Test
    public void testIntegrareCapete(){
        String t;
        op.integrare(p2, 1, 2);
        t = op.getResult().toString();
        assertNotNull(t);
        assertEquals(t, "+9.00");
        nrTesteCuSucces++;
    }

    @Test
    public void testDerivare1(){
        op.derivare(p1);
        String t = op.getResult().toString();
        assertNotNull(t);
        assertEquals(t, "+48.00x^3-18.00x^2+2.00");
        nrTesteCuSucces++;
    }
}
