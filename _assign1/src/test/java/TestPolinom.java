import Poli.*;
import org.junit.*;
import static org.junit.Assert.*;


public class TestPolinom {
    private static Polinom p;

    private static int nrTesteExecutate = 0;

    private static int nrTesteCuSucces = 0;


    public TestPolinom(){

    }

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        p = new Polinom();
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception{
        System.out.println("S-au executat " + nrTesteExecutate + "teste din care " + nrTesteCuSucces + " au avut succes!");
    }
    @Before
    public void setUp() throws Exception{
        System.out.println("Incepe un nou test!");
        p = new Polinom();
        nrTesteExecutate++;
    }
    @After
    public void tearDown() throws Exception{
        System.out.println("S-a terminat testul curent!");
    }

    @Test
    public void testGetGrad1(){
            try {
                p.parsePol("3x^12+2x-3");
                int t = p.getGradPolinom();
                assertNotNull(t);
                assertEquals(t, 12);
                nrTesteCuSucces++;
            }
            catch (Exception e){
            fail("Esuat!");
        }
    }

    @Test
    public void testToString1(){
        try {
            p.parsePol("7x^2+2x-1");
            String t = p.toString();
            assertNotNull(t);
            assertEquals(t, "+7.00x^2+2.00x-1.00");
            nrTesteCuSucces++;
        }
        catch (Exception e){
            fail("Esuat toString1!");
        }
    }

    @Test
    public void testParsePol1(){
        try {
            p.parsePol("3x^x");
            fail("Esuat parsePol1!");
        }
        catch (WrongFormatException ex){
            assertTrue(true);
            nrTesteCuSucces++;
        }
        catch (Exception e){
            fail("Esuat parsePol1-2!");
        }
    }

    @Test
    public void testParsePol2(){
        try {
            p.parsePol("3x^2+a");
            fail("Esuat parsePol2!");
        }
        catch (WrongFormatException ex){
            assertTrue(true);
            nrTesteCuSucces++;
        }
        catch (Exception e){
            fail("Esuat parsePol2-2!");
        }
    }

    @Test
    public void testParsePol3(){
        try {
            p.parsePol("3x+12-7x^5");
            String t = p.toString();
            assertNotNull(t);
            assertEquals("-7.00x^5+3.00x+12.00" ,t);
            nrTesteCuSucces++;
        }
        catch (Exception e){
            fail("Esuat testParsePol3!");
        }
    }
}
