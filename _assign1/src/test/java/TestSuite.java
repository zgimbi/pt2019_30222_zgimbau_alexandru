import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({
        TestPolinom.class,
        TestOperatii.class
})

public class TestSuite {

}
