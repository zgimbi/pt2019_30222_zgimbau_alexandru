package Controller;

import Poli.*;
import UI.*;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {

    private Operatii operatii;
    private UserInterface UI;

    public Controller(Operatii operatii, UserInterface UI){
        this.operatii = operatii;
        this.UI = UI;

        this.UI.addButonPlusListener(new PlusListener());
        this.UI.addButonMinusListener(new MinusListener());
        this.UI.addButonDerivareListener(new DerivareListener());
        this.UI.addButonIntegrareListener(new IntegrareListener());
        this.UI.addButonIntegrareCapeteListener(new IntegrareCListener());
        this.UI.addButonProdusListener(new ProdusListener());
        this.UI.addButonImpartireListener(new ImpartireListener());

    }


    class ImpartireListener implements ActionListener{
        public void actionPerformed(ActionEvent ev){
            Polinom p1, p2;
            RezImpartire res;
            try{
                p1 = new Polinom();
                p1.parsePol(UI.getFirstPol());
                p2 = new Polinom();
                p2.parsePol(UI.getSecondPol());
                res = operatii.impartire(p1, p2);
                UI.setTextAreaRezultat("Catul: " + res.getCat().toString() + "\n" + "Restul: " + res.getRest().toString());
            }
            catch (Exception exImp){
                JOptionPane.showMessageDialog(null, exImp);
            }
        }
    }

    class ProdusListener implements ActionListener{


        public void actionPerformed(ActionEvent e) {
            Polinom p1, p2;
            try{
                p1 = new Polinom();
                p1.parsePol(UI.getFirstPol());
                p2 = new Polinom();
                p2.parsePol(UI.getSecondPol());
                operatii.inmultire(p1, p2);
                UI.setTextAreaRezultat(operatii.getResult().toString());
            }
            catch (Exception exProd){
                JOptionPane.showMessageDialog(null, exProd);
            }
        }
    }

    class IntegrareCListener implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            Polinom p;
            try{
                p = new Polinom();
                p.parsePol(UI.getFirstPol());
                operatii.integrare(p, UI.getA(), UI.getB());
                UI.setTextAreaRezultat(operatii.getResult().toString());
            }
            catch (Exception exIntCap){
                JOptionPane.showMessageDialog(null, exIntCap);
            }
        }
    }
    class IntegrareListener implements ActionListener{


        public void actionPerformed(ActionEvent e) {
            Polinom p;
            try {
                p = new Polinom();
                p.parsePol(UI.getFirstPol());
                operatii.integrare(p);
                UI.setTextAreaRezultat(operatii.getResult().toString());
            }
            catch (Exception eInteg){
                JOptionPane.showMessageDialog(null, eInteg);
            }

        }
    }
    class PlusListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            Polinom p1;
            Polinom p2;
            try{
                p1 = new Polinom();
                p1.parsePol(UI.getFirstPol());
                p2 = new Polinom();
                p2.parsePol(UI.getSecondPol());
                operatii.suma(p1, p2);
                UI.setTextAreaRezultat(operatii.getResult().toString());
            }
            catch (Exception ex){
                JOptionPane.showMessageDialog(null, ex);
            }

        }
    }
    class MinusListener implements ActionListener{
        public void actionPerformed(ActionEvent evMinus){
            Polinom p1, p2;
            try{
                p1 = new Polinom();
                p1.parsePol(UI.getFirstPol());
                p2 = new Polinom();
                p2.parsePol(UI.getSecondPol());
                operatii.diferenta(p1, p2);
                UI.setTextAreaRezultat(operatii.getResult().toString());
            }
            catch (Exception ex){
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    class DerivareListener implements ActionListener{
        public void actionPerformed(ActionEvent evDer){
            Polinom p;
            try{
                p = new Polinom();
                p.parsePol(UI.getFirstPol());
                operatii.derivare(p);
                UI.setTextAreaRezultat(operatii.getResult().toString());
            }
            catch (Exception ex){
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

}
