package Poli;

public class Monom {
    private int grad;
    private float coef;
    private int used;

    public Monom(){}
    public Monom(int grad, float coef){
        this.grad=grad;
        this.coef=coef;
    }
    public int getGrad(){
        return grad;
    }
    public void setGrad(int gr) {
        grad=gr;
    }
    public void setCoef(float c){
        coef=c;
    }
    public void notCoef(){
        this.coef = -coef;
    }
    public float getCoef(){
        return coef;
    }
    public void setUsed(){
        used = '1';
    }
    public int getUsed(){
        return used;
    }
    public String getMonom(){
        if(coef == 0 || grad == 0)
            return "";
        else if(coef>0)
            return "+"+coef+"x^"+grad;
        else
            return coef+"x^"+grad;
    }
    @Override
    public String toString(){
        String s = "";
        if(coef >0){
            s = "+" + s + String.format("%.2f", coef);
        }
        else if(coef !=0)
        s = s + String.format("%.2f", coef);
        if (coef == 0){
            s="";
            return s;
        }
        if(grad>1){
            s = s +"x^"+grad;
        }
        else if(grad == 1)
            s = s + "x";

        return s;
    }
}
