package Poli;

public class RezImpartire {
    private Polinom cat;
    private Polinom rest;

    public RezImpartire(Polinom cat, Polinom rest){
        this.cat = cat;
        this.rest = rest;
    }
    public Polinom getRest() {
        return rest;
    }
    public Polinom getCat(){
        return cat;
    }
}
